---
title: Liquid-Ion Gating of MoS~2~
subtitle: Problems, Solutions and Open Questions
abstract: TODO
format:
  pdf:
    fig-pos: ht
    template: ../misc/templates/thesis.latex
    titlepage: true
    footnotes-pretty: true
    toc-depth: 2
---

```{r setup}
here::i_am("notebooks/mos2-liquid-ion-gating.qmd")
source(here::here("utils/misc.R"))

options(
  "scipen" = 2,
  "digits" = 3
)
knitr::opts_chunk$set(
  echo = TRUE
)
```

```{r load-packages}
library(RColorBrewer)
library(patchwork)
library(tidyverse)
library(units)
library(viridis)
library(here)
library(janitor)

theme_set(theme_linedraw())
```
	
# Samples 
## Fabrication

Si/SiO~2~ wafers (4.5x4.5 mm) where first cleaned using acetone in an ultrasonic bath. 
Afterwards the wafers where placed in acetone again, then flushed with isopropyl alcohol, and finaly blown dry using a nitrogen gun.

After cleaning the MoS~2~ flakes where transferred onto TODO *TRANSPARENT STICKY STUFF* using TODO *BLUE TAPE*. 
The flakes could then be placed onto the wafers under an optical microscope.

Lithography was performed using a [Heidelberg Instruments µMLA](https://heidelberg-instruments.com/product/%CE%BCmla/) and S1813 photo resist with LOR3 for better lift-off. The resist was developed with TODO. Lift-off was done using Remover PG. 

Contact layers where deposited using electron-beam physical vapor deposition in two steps. First flake contacts made from Gold on Bismuth where created. Subsequently Gold on Titanium traces and bondpads where deposited.

For some samples a protective layer of S1813 was applied where windows over the flakes where created using optical lithography.
	
## Design

Every sample contained three MoS~2~ flakes near the center of the Si/SiO~2~ wafer. 
Every flake was contacted with large source/drain contacts at the ends of the flakes. 
Inbetween two to four small electrodes for voltage probing where deposited depending on the size of the flake.

Near the flakes in the center of the sample a big Ti/Au electrode was placed to act as the liquid-ion gate. 

Bondpads where put at the edge of the samples. This configuration allowed applying a drop of DEME-TFSI to the center of the sample without it touching the bond wires.

# DEME-TFSI Gating at Room Temperature

```{r data-2d}
df.2dsweep <-
  graffl::read_labmoose_data(file = here::here(dir.measure, "ksij/liquid-test-2d-sweep/data.dat"))

max_current <-
  df.2dsweep |>
  pull(sample_current) |>
  max()
```

```{r}
df.2dsweep.plot <-
  df.2dsweep |>
  pivot_longer(
    cols = c(gate_current, sample_current),
    names_to = "contact",
    values_to = "current"
  ) |>
  mutate(contact = graffl::snake_to_title(contact))
```

```{r fig-plot-2d-sweep}
#| fig-cap: TODO

plot.2dsweep <-
  df.2dsweep.plot |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = sample_voltage,
    fill = drop_units(current),
    group = contact
  ) +
  geom_tile() +
  scale_fill_viridis() +
  facet_wrap(
    facets = vars(contact),
    ncol = 1
  ) +
  labs(
    title = "2D-Sweep of Gate- and Bias-Voltage",
    subtitle = "Sample: KSIJ-A",
    x = "Gate Voltage",
    y = "Bias Voltage",
    fill = "Current [A]"
  )

plot.2dsweep
```

```{r fig-plot-gatetrace}
#| fig-cap: Liquid-ion gatesweep.
plot.gatetrace <-
  df.2dsweep.plot |>
  filter(sample_voltage == as_units(0, "V")) |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = current,
    color = contact
  ) +
  geom_line() +
  scale_y_units(unit = "uA") +
  labs(
    x = "Gate Voltage",
    y = "Current",
    title = "Gatetrace of DEME-TFSI Gated Sample",
    subtitle = "Sample: KSIJ-A; Bias Voltage: 0V"
  ) +
<<<<<<< HEAD
  theme(legend.position = "none") +
  facet_wrap("contact", ncol = 1)
=======
  theme(
    legend.position = c(0.2, 0.8),
    legend.title = element_blank(),
    legend.background = element_blank()
  )
```

```{r fig-gatetrace}
#| fig-cap: Liquid-ion gatesweep.
>>>>>>> 5035d63654035e2dcf4fe68f53f6cf64c6a2efb8

plot.gatetrace
```

```{r}
df.decay <-
  graffl::read_labmoose_data(
    here::here(dir.measure, "ksij/current-decay/data.dat")
  ) |>
  select(-minutes) 
```

```{r fig-plot-current-decay}
#| fig-cap: Decay of charge accumulation.
plot.decay <-
  df.decay |>
  mutate(duration = as_units(time - min(time), "s")) |>
  pivot_longer(
    cols = c(gate_current, sample_current),
    names_to = "contact",
    values_to = "current"
  ) |>
  mutate(contact = graffl::snake_to_title(contact)) |>
  ggplot() +
  aes(
    x = duration,
    y = current,
    color = contact
  ) +
  geom_line() +
  scale_x_units(unit = "min") +
  scale_y_units(unit = "uA") +
  coord_trans(xlim = c(NA, 5)) +
  labs(
    x = "Time",
    y = "Current",
    title = "Decay of Charge Accumulation at Room Temperature",
<<<<<<< HEAD
    subtitle = "Sample: KSIJ-A; Gate Voltage before switch-off: 4V"
=======
    subtitle = "Log of Absolute Current; Sample: KSIJ-A; Gate Voltage before switch-off: 4V"
    ) +
  theme(
    legend.position = c(0.8, 0.9),
    legend.title = element_blank(),
    legend.background = element_blank()
>>>>>>> 5035d63654035e2dcf4fe68f53f6cf64c6a2efb8
  ) +
  theme(
    legend.title = element_blank(),
    legend.position = c(0.8, 0.2)
  )

plot.decay
```

# Problems 

## DEME-TFSI destroys Sample
### Problem {-}
DEME-TFSI applied to a sample can corrode and destroy the metal contacts, as well as the MoS~2~ flakes themselves, when applying a voltage to the liquid-ion gate.

::: {#fig-degradation-1 layout-ncol=2}

![Before DEME-TFSI](../data/images/kshf/optical/after-metal-deposition/png/test0005.png){#fig-degradation-1-a}

![After DEME-TFSI](../data/images/kshf/optical/after-deme-tfsi/png/kshf-degradation-16.png){#fig-degradation-1-b}

Degradation of KSHF-B. After measuring at cryogenic temperatures the contacts, traces and flakes of sample KSHF were destroyed by DEME-TFSI
:::

::: {#fig-degradation-2 layout-ncol=2}

![KSIJ before DEME-TFSI](../data/images/ksij/optical/finished/Snap_404.jpg){#fig-degradation-2-a}

![KSIJ after DEME-TFSI](../data/images/ksij/optical/after-deme/Snap_502.jpg){#fig-degradation-2-b}

![KSIJ-A before DEME-TFSI](../data/images/ksij/optical/finished/Snap_455.jpg){#fig-degradation-2-c}

![KSIJ-A after DEME-TFSI](../data/images/ksij/optical/after-deme/Snap_504.jpg){#fig-degradation-2-d}

Degradation of KSIJ. 
The sample was measured for an extensive amount of time at room temperature. 
The applied gate voltages ranged from 0 to 5 Volts with a maximum measured leak current of `r round(set_units(max_current, "uA"), digits = 2)` $\mu$A at the MoS~2~ flake KSIJ-A.
:::

### Possible Solutions {-}

  - Keep the leakage current that occurs due to the electro-chemical reaction at or below 1nA.
  - Only apply a voltage to the liquid-ion gate at temperatures close to 220K.
  - Apply a protective resist layer on top of the sample to only expose the flakes themselves to the ionic liquid. 
  This was done for KSIJ, but excessive leakage currents still destroyed the protective S1813 layer.
  For lower currents we did not observe damage to the protective resist layer (Sample KSAB).

## Protective Resist Layer Cracks at Low Temperatures
### Problem {-}

The final resist layer is meant to protect the sample from DEME-TFSI and make sure that primarily the flakes themselves accumulate carriers on their surface, but cracks at low temperatures.
The Au/Cr traces leading from the bondpads to the flake contacts are not affected by this. The Au/Bi flake contacts on the other hand are too soft to withstand the occuring forces and break along the cracks of the resist.


::: {#fig-cracks layout-ncol=1}

![KSAB-A before cooldown](../data/images/ksab/optical/finished/Snap_398.jpg){#fig-cracks-a}

![KSAB-A after cooldown (with DEME-TFSI drop still on the sample)](../data/images/ksab/optical/after-cooldown/Snap_424.jpg){#fig-cracks-b}

![KSAB-A after cooldown](../data/images/ksab/optical/after-cooldown/Snap_446.jpg){#fig-cracks-c}

Degradation of KSIJ. 
The sample was measured for an extensive amount of time at room temperature. 
The applied gate voltages ranged from 0 to 5 Volts with a maximum measured leak current of `r round(set_units(max_current, "uA"), digits = 2)` $\mu$A at the MoS~2~ flake KSIJ-A.
:::

```{r}
df.crack <-
  read_tsv(
    here::here("data/measurements/ksab/gatetrace/2023-08-10_19-39-23_KSAB_Vsd#18#21_Vg#10_gatetrace_001/data.dat")
  ) |>
  rename(
    "gate_voltage" = "# voltage",
    "gate_current" = "gatecurrent",
    "sample_current" = "current"
  ) |>
  mutate(
    gate_voltage = as_units(gate_voltage, "V"),
    gate_current = as_units(gate_current, "A"),
    sample_current = as_units(sample_current, "A")
  )
```

```{r fig-plot-crack}
#| fig-cap: TODO

df.crack |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = sample_current,
  ) +
  geom_line() +
  coord_trans(
    xlim = c(NA, 10)
  ) +
  labs(
    x = "Gate Voltage",
    y = "Flake Current",
    title = "Gatesweep",
    subtitle = "Sample: KSAB"
  )
```

### Possible Solutions {-}

  - Don't use a protective resist layer. Duh!
  - Try a different photo resist (e.g. S1805).
  - Increase the size of the "windows" to include the Bi/Au contacts.
  
# Current Plans

  - Fabricate samples with larger "windows" in the resist (KSJJ, KSLJ).
  - Sweep the voltage applied to the liquid-ion gate at ~220K until a leak current of ~1nA is reached and hold the voltage while cooling down slowly.
  - Measure 4-Point-resistance.
  
# Open Questions

  - How long to apply a voltage to the liquid-ion-gate before cooling down below 220K?
  - Should the voltage be applied during the whole measuring process or can it be turned off after reaching cryogenic temperatures?
