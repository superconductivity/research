---
title: "KSTH-A"
subtitle: "DEME-TFIS Training"
author: "Konstantin D. Schneider"
format: 
  html:
    df-print: paged
    theme: cosmo
execute:
  echo: false
  warning: false
  messages: false
---

```{r}
library(tidyverse)
library(here)
```

```{r read-data}
df <-
  read_tsv(
    here::here("data/measurements/ksth/deme-training/KSTH-A_training.dat")
  ) |>
  arrange(time) |>
  mutate(
    time = lubridate::as_datetime(time),
    gate_voltage = round(gate_voltage, digits = 2),
    direction = case_when(
      c(NA, diff(gate_voltage)) > 0 ~ "Up",
      c(NA, diff(gate_voltage)) < 0 ~ "Down",
      TRUE ~ "constant"
    )
  ) 

adjust_direction <- function(direction) {
  for (i in seq_along(direction)) {
    if (is.na(direction[i]) == TRUE && i == 0) {
      direction[i] <- "Up"
    }

    if (direction[i] == "constant" && i < length(direction)) {
      j <- i + 1
      while (j <= length(direction) && direction[j] == "constant") {
        j <- j + 1
      }
      if (j <= length(direction)) {
        direction[i] <- direction[j]
      }
    }
  }
  return(direction)
}

df$direction <- adjust_direction(df$direction)

# Initialize a new column for segments
df$segment <- NA

# Create segments based on the trend pattern
segment_id <- 0

for (i in 1:nrow(df)) {
  if(df$direction[i] == "Up" && df$gate_voltage[i] == 0) {
    segment_id <- segment_id + 1
  }

  df$segment[i] <- segment_id

}

df <- 
  df |>
  group_by(segment) |>
  mutate(max_gate_voltage = max(gate_voltage)) |>
  filter(max_gate_voltage != 0) |>
  ungroup()
```

# Plots

## Row

### {.tabset}

```{r}
#| title: Direction

df |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = device_current,
    color = direction,
    group = max_gate_voltage
  ) +
  geom_path() +
  labs(
    x = "Gate Voltage [V]",
    y = "Device Current [A]",
    color = "Direction"
  ) +
  theme(
    legend.position = "top"
  )

ggsave(here::here("plots/ksth-a-deme-training-device-direction.pdf"))
```

```{r}
#| title: Max. Gate Voltage

plot <- 
  df |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = device_current,
    color = as_factor(max_gate_voltage)
  ) +
  geom_path() +
  labs(
    x = "Gate Voltage [V]",
    y = "Gate Current [A]",
    color = "max. GV [V]"
  )

plotly::ggplotly() |>
  htmlwidgets::saveWidget(here::here("plots/ksth-a-deme-training-device-traces.html"))
```

### {.tabset}

```{r}
#| title: Direction

df |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = gate_current,
    color = direction,
    group = max_gate_voltage
  ) +
  geom_path() +
  labs(
    x = "Gate Voltage [V]",
    y = "Gate Current [A]",
    color = "Direction"
  ) +
  theme(
    legend.position = "top"
  )

ggsave(here::here("plots/ksth-a-deme-training-gate-direction.pdf"))
```

```{r}
#| title: Max. Gate Voltage

df |>
  ggplot() +
  aes(
    x = gate_voltage,
    y = gate_current,
    color = as_factor(max_gate_voltage),
    group = max_gate_voltage
  ) +
  geom_path() +
  labs(
    x = "Gate Voltage [V]",
    y = "Gate Current [A]",
    color = latex2exp::TeX("max. $V_G$")
  )
```

## Row 2 

```{r}
df |>
  group_by(max_gate_voltage) |>
  summarise(
    max_gate_current = max(gate_current),
    min_gate_current = min(gate_current),
    mean_gate_current = mean(gate_current),
    max_device_current = max(device_current),
    min_device_current = min(device_current),
    mean_device_current = mean(device_current) 
  ) |>
  pivot_longer(max_device_current:mean_device_current) |>  
  mutate(
    name = case_when(
      name == "max_device_current" ~ "Max",
      name == "min_device_current" ~ "Min",
      name == "mean_device_current" ~ "Mean"
    )
  ) |>
  ggplot() +
  aes(
    x = max_gate_voltage,
    y = value,
    color = name,
    shape = name
  ) +
  geom_line() +
  geom_point() +
  labs(
    x = "Gate Voltage [V]",
    y = "Gate Current [A]",
  ) +
  theme(
    legend.position = "top",
    legend.title = element_blank()
  )
```

```{r}
df |>
  group_by(max_gate_voltage) |>
  summarise(
    max_gate_current = max(gate_current),
    min_gate_current = min(gate_current),
    mean_gate_current = mean(gate_current),
    max_device_current = max(device_current),
    min_device_current = min(device_current),
    mean_device_current = mean(device_current) 
  ) |>
  pivot_longer(max_gate_current:mean_gate_current) |>  
  mutate(
    name = case_when(
      name == "max_gate_current" ~ "Max",
      name == "min_gate_current" ~ "Min",
      name == "mean_gate_current" ~ "Mean"
    )
  ) |>
  ggplot() +
  aes(
    x = max_gate_voltage,
    y = value,
    color = name
  ) +
  geom_line() +
  geom_point() +
  labs(
    x = "Max. Gate Voltage [V]",
    y = "Device Current [A]",
  ) +
  theme(
    legend.position = "top",
    legend.title = element_blank()
  )
```

# Data

```{r}
df |>
  select(
    "Time" = time,
    "Gate Voltage [V]" = gate_voltage,
    "Gate Current [A]" = gate_current,
    "Device Current [A]" = device_current,
    "Direction" = direction,
    "Max. Gate Voltage" = max_gate_voltage
  ) |>
  DT::datatable()
```

```{r}
df |>
  group_by(max_gate_voltage) |>
  summarise(
    "Max. Gate Current" = max(gate_current),
    "Min. Gate Current" = min(gate_current),
    "Mean Gate Current" = mean(gate_current),
    "Max. Device Current" = max(device_current),
    "Min. Device Current" = min(device_current),
    "Mean Device Current" = mean(device_current) 
  ) |>
  rename("Max. Gate Voltage" = max_gate_voltage) |>
  DT::datatable()
```
