---
title: Superconduction in MoS~2~ Nanotubes
date: 2023-06-15
format:
  revealjs:
    width: 1024
    height: 768
---

# Overview

- Theory
  - Superconduction in MoS~2~
  - Liquid Ion Gating
- Sample: KSHF
  - Fabrication
  - 2-Way Resistances
  - "Measurement"
- Future Plans/Current Work

# Theory
## Why interesting?
## How it could be achieved?
### Liquid-ion gating.

# Previous Works

# Sample: KSHF
> A tragedy in three acts.

## Fabrication
## Flakes

::: {layout-ncol=2}

![Flake A](../data/kshf/images/optical/flakes/hf-flakes-0017.jpg)

![Flake B](../data/kshf/images/optical/flakes/hf-flakes-0033.jpg)

![Flake D](../data/kshf/images/optical/flakes/hf-flakes-0011.jpg)

![Flake E](../data/kshf/images/optical/flakes/hf-flakes-0014.jpg)

:::

## Structure

## 2-Way Resistances
Setup:

```{mermaid}
flowchart LR
  A[Yokogawa 1..10 mV] --> B[Probe Station] 
  B --> D{{Sample}} --> B --> E[Blackbox e-6] --> F[Keithley]
```

- Something about the measurement.

## 2-Way Resistances

```{r}
library(tidyverse)
library(kableExtra)
```

```{r}
data <-
  # read data
  readr::read_csv(
    here::here("data/measurements/kshf/contacts.csv")
  ) |>  
  # calculate output current
  mutate(current = voltage_out * amplification) |>         
  # calculate resistance
  mutate(resistance = voltage_in / current) |>
  # change flakes numbering to letters
  mutate(
    flake = letters[flake] |>
      stringr::str_to_upper()
  ) |>
  # adjust contact name
  mutate(
    contact_1 = contact_1 %% 10,
    contact_2 = contact_2 %% 10
  )
```

```{r}
data |>
  mutate(
    voltage_in = as_factor(voltage_in * 10^3)
  ) |>
  group_by(flake, voltage_in) |>
  summarise(mean_resistance = mean(resistance)) |>
  ggplot2::ggplot() +
  ggplot2::aes(x = as_factor(flake), y = mean_resistance * 10^-3, fill = voltage_in) +
  ggplot2::geom_col(position = "dodge", color = "black") +
  ggplot2::scale_fill_brewer(palette = "Set2") +
  ggplot2::labs(
    title = "Mean Resistance by Flake",
    subtitle = "KSHF",
    x = "Flake",
    y = latex2exp::TeX("Resistance in $k\\Omega$"),
    fill = latex2exp::TeX("Input Voltage in $mV$")
  ) +
  ggplot2::theme_minimal() +
  ggplot2::theme(legend.position = "top")
```

## 2-Way Resistances

```{r}
data |>
  ggplot(
    aes(
      x = distance * 10^6,
      y = resistance * 10^-3
    )
  ) +
  geom_smooth(method = "lm", alpha = 0.2, se = TRUE, color = "darkgrey") +
  geom_point(aes(color = as_factor(voltage_in * 10^3))) +
  ggplot2::labs(
    title = "Resistance vs. Distance",
    subtitle = "KSHF",
    x = latex2exp::TeX("Distance in $\\mu m$"),
    y = latex2exp::TeX("Resistance in $k\\Omega$"),
    color = latex2exp::TeX("Input Voltage in $mV$")
  ) +
  ggplot2::scale_color_brewer(palette = "Set2") +
  ggplot2::theme_minimal() +
  ggplot2::theme(legend.position = "top") +
  facet_wrap(vars(flake), ncol = 2)
```

## 2-Way Resistances

```{r}
table <-
  data |>
  group_by(flake, contact_1, contact_2) |>
  mutate(sd_resistance = sd(resistance)) |>
  group_by(flake) |>
  summarise(
    mean_resistance = mean(resistance),
    mean_sd_resistance = mean(sd_resistance)
  ) |>
  mutate(
    mean_resistance = round(mean_resistance * 10^-3, digits = 1),
    mean_resistance = glue::glue("{mean_resistance} kOhm"),
    mean_sd_resistance = round(mean_sd_resistance * 10^-3, digits = 1),
    mean_sd_resistance = glue::glue("{mean_sd_resistance} kOhm"),
  ) |>
  knitr::kable(
    col.names = c("Flake", "Mean Resistance", "Mean SD")
  )
```

```{r}
table
```

## 2-Way Resistances

```{r}
table <-
  table |>
  row_spec(4, strikeout = TRUE)
```

```{r}
table
```

## "Measurement"
## Aftermath
### Possible Reasons
### Possible Ways to Avoid

# Current Plans

