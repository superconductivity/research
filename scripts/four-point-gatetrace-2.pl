
use Lab::Moose;
use Time::HiRes qw /time/;
use 5.36.0;
use PDL;
# Sample

use lib "C:/Users/Demag/Desktop/Regensburg 2/lib";

my $sample = 'KSIJ-A';  		# chip name

# Setup ---

my $sensitivity = -1e-8; 	# sensitivity of DL1211 in A/V
my $rise_time = 100;       	# rise time in ms

## Bias Voltage
my $bias_voltage = 0.5;        # voltage set to biasyokogawa

## Parameters of the Trace
my $gate_start= 0.0;
my $gate_end  = 1.0;
my $gate_step = 0.005;

## Instruments
### Bias
my $bias_source = instrument(
    type => 'Yokogawa7651',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 3},
    max_units_per_step => 0.1,
    max_units_per_second => 0.1,
    min_units => -10,
    max_units => 10,
);

### Gate
my $gate_source = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {gpib_address => 1},
    max_units_per_step => 0.01,
    max_units_per_second => 0.1,
    min_units => -10,
    max_units => 10,
);

### Multimeter
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);

## Sweep
my $gate_sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gate_source,
    from => $gate_start, to => $gate_end, step => $gate_step
);

# Data file
my $data_file = sweep_datafile(columns => [qw/gate_voltage current time bias_voltage/]);
$data_file -> add_plot(
  x => 'gate_voltage',
  y => 'current'
    );

# Measurement
my $measurement = sub {
  my $sweep = shift;
 
  my $current_out = $multimeter -> get_value();
  my $current = $current_out * $sensitivity;
 
  $sweep -> log(
    gate_voltage => $gate_source -> cached_level(),
    current => $current,
    time => time(),
    bias_voltage => $bias_voltage
      );
};

# Run
$gate_source -> set_level(value => $gate_start);
$bias_source -> set_level(value => $bias_voltage);

$gate_sweep -> start(
    measurement => $measurement,
    datafile    => $data_file,
    folder => 'test',
    date_prefix => 1,
);
