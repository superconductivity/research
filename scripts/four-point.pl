use 5.36.0;
use Lab::Moose;

my $voltage_source = instrument(
        type => 'YokogawaGS200',
        connection_type => 'LinuxGPIB',
        connection_options => {pad => 1}
    )
