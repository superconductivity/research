#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: 4-Point Measurement @ Room Temperature

use Lab::Moose;
use 5.010;

use lib '/home/user/Robin/RSEJ/Dilfridge';

# Sample

my $sample  = 'KSHF_';  # chip name
my $PINsI   = 'Vsd#19#4_';    # pins / cables at chipcarrier
my $PINGate = 'Vg#11_';     # pins / cables at chipcarrier

# parameters of the setup

my $lineresistance = 100; # resistance of measurement line (Ohm)
my $sensitivity = -1e-8; # sensitivity of DL1211 in A/V
my $risetime = 100e-3;       # rise time in ms

my $biasvoltage = 0.01;   # voltage set to biasyokogawa
my $divider = 1;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $TEMP = 4.2;

# parameters of the gate trace
my $gatestart   = 20;
my $gateend     = 0;
my $gatestep    = 0.05;

# instruments

my $multimeter = instrument(
    type => 'HP3458A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);
$multimeter->set_nplc(value => '1');

my $biasyoko = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.2,
    max_units_per_second => 5,
    min_units => -30,
    max_units => 30,
);

my $gateyoko = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.02,
    max_units_per_second => 0.2,
    min_units => -32,
    max_units => 32,
);
$gateyoko->sense_nplc(value => '0.01');

# sweep
my $sweep = sweep(
    type        => 'Step::Voltage',
    instrument  => $gateyoko,
#    delay_in_loop => 0.05,
    from => $gatestart, to => $gateend, step => $gatestep,
    backsweep   => 0,
        delay_before_loop  => 0,
        delay_in_loop      => $risetime,
);

# data file

my $datafile = sweep_datafile(columns => [qw/voltage current gatecurrent/]);

my $format_bias = sprintf('%.3f', $samplebias * 1e3);
# plot 
$datafile->add_plot(
        x => 'voltage',
        y => 'current',
        plot_options => {
                title     => "Gatesweep - $sample$PINsI - $PINGate - U_Bias = $format_bias mV - T_stick = $TEMP K",
                xlabel    => 'U_G [V]',
                ylabel    => 'I [A]',
                format    => {x => "'%.1f'", y => "'%.2e'"},
                grid      => 0,
#               logscale  => [xy=>10],
        },
        curve_options => {
                with => 'lines',
                linetype => 2,
                linecolor => 'black',
                linewidth => 2,
        }
);
$datafile->add_plot(
    x => 'voltage',
    y => 'gatecurrent',
    hard_copy_suffix => '_gatecurrent',
);

# measurement

my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter->get_value();
  my $i_dc = $ampvoltage*$sensitivity;
  my $gatecurr = $gateyoko->get_measurement()->{CURR};
 
  $sweep->log(
        voltage => $gateyoko->cached_level(),
        current => $i_dc,
        gatecurrent=> $gatecurr,
  );
};

# run it

$biasyoko->set_level(value => $biasvoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder      => $PINsI.'gatetrace',
    date_prefix => 1,
);