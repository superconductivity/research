#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use 5.010;

use lib '/home/user/Robin/RSEJ/Dilfridge';

# parameters of the setup

my $biasyoko = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 5,
    max_units_per_second => 10,
    min_units => -30,
    max_units => 30,
);

my $gatekeith = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 0.5,
    min_units => -30,
    max_units => 30,
);

# run it
$biasyoko->set_level(value => 0);
$gatekeith->set_level(value => 0);
