#-----------------------------------#
# 4-Point Measurement (MoS2 Flakes) #
#-----------------------------------#

#!/bin/perl

use Lab::Moose;
use Time::HiRes qw(time);
use 5.36.0;
use PDL;

# Fixed Parameters ---
my $rise_time = 100; # Rise time in ms
my $gate_end  = 10; # Final voltage of sweep
my $gate_start= -10; # Start voltage of sweep
my $gate_step = 0.01; # Sweep step size

# Parameters ---
my $sample; 
my $bias_voltage; # Voltage set at bias source
my $sensitivity; # Sensitivity of DL1211 in A/V

## Instruments
my $bias_source = instrument(
    type => 'Yokogawa7651',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 3},
    max_units_per_step => 0.1,
    max_units_per_second => 0.1,
    min_units => -10,
    max_units => 10,
    );

my $gate_source = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {gpib_address => 1},
    max_units_per_step => 0.01,
    max_units_per_second => 0.01,
    min_units => -10,
    max_units => 10,
    );

my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
    );

# Sweep ---
my $gate_sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gate_source,
    from => $gate_start, to => $gate_end, step => $gate_step
    );

# Data File ---
my $datafile = sweep_datafile(
    columns => [qw(gate bias current voltage sensitivity time)]
    );

## Plot ---
$datafile -> add_plot(
    x => 'gate_voltage',
    y => 'current',
    title => '4-Point Gatesweep of ${sample}'
    );

# Measurement ---
my $t0 = time()
my $measurement = sub {
    my $sweep = shift;
    my $bias_voltage_in = $bias_source -> get_value();
    my $probe_voltage = $multimeter -> get_value();
    my $gate_voltage = $gate_source -> cached_level();
    
    $sweep -> log(
	datafile => $data_file_raw,
	gate_voltage => $voltage,
	current_out => $current,
	time => time() - $t0,
	bias_voltage => $bias_voltage,
	);
};

# Run
$gate_source -> set_level(value => $gate_start);
$bias_source -> set_level(value => $bias_voltage);

$gate_sweep -> start(
    measurement => $measurement,
    datafile    => [$data_file_raw, $data_file_processed],
    folder => 'four-point-gatetrace',
    date_prefix => 1,
    );
