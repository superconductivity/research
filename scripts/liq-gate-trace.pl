#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use 5.010;

# Sample

my $sample = 'KSAB_';  # chip name
my $PINGate = 'VLG#8_';    # pins / cables

# parameters of the setup

my $risetime = 0.100;       # rise time in s

my $biasvoltage = 1.0;   # voltage set to biasyokogawa
my $divider = 100;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

# parameters of the gate trace
my $gatestart = 0;
my $gateend = 5;
my $gatestep = 0.002;
my $gateDelay = 0; # s
# my $gateInDelay = 0.5 * $risetime; # s
my $gateInDelay = 0; # s
my $gatebackswp = 0; # Do a backsweep (1) or not (0)

# instruments
my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 19},
    input_channel => 'A', # set default input channel for all method calls
);
my $TEMP = $lakeshore->get_sensor_units_reading();
 
my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 0.1,
    min_units => -100,
    max_units => 100,
);
$gatesource->sense_function_on(value => ['CURR']);
$gatesource->sense_nplc(value => 1);

# sweep
my $sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gatestart, to => $gateend, step => $gatestep,
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

# data file

my $datafile = sweep_datafile(columns => [qw/voltage gatecurrent temp/]);

# plot 
$datafile->add_plot(
	x => 'voltage',
	y => 'gatecurrent',
	plot_options => {
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	live => $livePlot,
);

# measurement
my $meas = sub {
  my $sweep = shift;
 
  my $gatecurrent = $gatesource->get_measurement()->{CURR};
 
  $sweep->log(
	  voltage => $gatesource->cached_level(),
	  gatecurrent => $gatecurrent,
	  temp => $lakeshore->get_sensor_units_reading(),
  );
};

# run it
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> $sample.$PINGate.'liqgatetrace',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
#$gatesource->set_level(value => 0);
#$biassource->set_level(value => 0);
